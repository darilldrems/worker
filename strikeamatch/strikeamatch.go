/**
*@author Ridwan Olalere
*
*
*/
package strikeamatch

import (
	"strings"
	"fmt"
)

func letterPairs(str string) []string{
	numPairs := len(str) - 1
	pairs := make([]string, numPairs)
	
	arrString := strings.SplitN(str, "", len(str))	
	
	fmt.Println("arrString", arrString)

	for i := 0; i < numPairs; i++{
		lo := i
		hi := i + 1
		pair := arrString[lo] + arrString[hi]
		
		pairs[i] = pair
	}

	
	return pairs

}


func wordLetterPairs(str string) []string{
	var allPairs []string

	allWords := strings.Fields(str)
	
		
	for w := 0; w < len(allWords); w++{
		pairsInWord := letterPairs(allWords[w])
		for i := 0; i < len(pairsInWord); i++{
			allPairs = append(allPairs, pairsInWord[i])
		}
	}
	
	
	return allPairs
}

/**
*Returns the similarity ranking of two strings
*
*@param str1 the first string to compare
*@param str2 the second string to compare withg str1
*@return the similarity ranking ranking from 0 - 1
*
*/
func compareStrings(str1 string, str2 string) float64{
	
	pairs1 := wordLetterPairs(strings.ToLower(str1))
	pairs2 := wordLetterPairs(strings.ToLower(str2))

	
	intersection := 0
	union := float64(len(pairs1) + len(pairs2))

	for i := 0; i < len(pairs1); i++{
		pair1 := pairs1[i]

		for j := 0; j < len(pairs2); j++{
			
			pair2 := pairs2[j]
			if pair1 == pair2 {
				intersection++
				pairs2 = append(pairs2[:j], pairs2[j+1:]...)
				break
			}
			
			
		}			
		
		
	}
	
	return (2.0 * float64(intersection))/union

}

func existingWordsScore(str1, str2 string) float64{
	words1 := strings.Split(strings.ToLower(str1), " ")
	words2 := strings.Split(strings.ToLower(str2), " ")

	size := float64(len(words1))
	score := 0

	for _, word := range words1{
		for j, word2 := range words2{
			if word == word2{
				score = score + 1
				words2 = append(words2[:j], words2[j+1:]...)
				break
			}
		}
	}

	return float64(score)/size
}


func SimilarityScore(str1, str2 string) float64{
	
	score1 := compareStrings(str1, str2)
	score2 := existingWordsScore(str1, str2)

	if score2 == 1.0{
		return 1.0
	}

	return (score1/score2)/2.0
}

// func main(){
//    fmt.Println(SimilarityScore("iPhone 6", "apple iphone 6 iphone"))
// }