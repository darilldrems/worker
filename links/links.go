/**
*@author Ridwan Olalere
*@website http://bestprices.ng
*/
package links

import (
    "fmt"
    "github.com/PuerkitoBio/goquery"
    "regexp"
)

func formatUrl(url string){

    absoluteUrl := "^(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$"
//    relativeUrl := "^([\\/\\w \\.-]*)*\\/?"
    
    is_valid, _ := regexp.MatchString(absoluteUrl, url)
    
    if !is_valid{
//        fmt.Println("Not valid man")
    }
    
//    fmt.Println(url)
}


func GetLinks(website string) []string{
    retrievedLinks := make([]string, 0)
    
    doc, err := goquery.NewDocument(website)
    
    if err != nil{
        return []string{}
    }
    
    doc.Find("a").Each(func(i int, s *goquery.Selection){
        link, _ := s.Attr("href")
        
        formatUrl(link)
      
        retrievedLinks = append(retrievedLinks, link)
    })
    
    fmt.Println(retrievedLinks)
    return retrievedLinks
}

//func main(){
//
//    GetLinks("http://www.slot.ng/catalogsearch/result/?q=apple")
//
//}