package product
/**
*@author Ridwan Olalere
*Uses microdata or common class names or xpath to extract product information from html web page
*
*
*/


import (
	"fmt"
	"microdata"
//	"reflect"
//    "strings"
    "strconv"
    "github.com/PuerkitoBio/goquery"
    "regexp"
)

type Product struct{
	Price float64
	Name string
	Link string
	
}


func cleanTitle(val string) string{
    
    reg, _ := regexp.Compile("[^A-Za-z0-9]")
    
    return reg.ReplaceAllString(val, " ")
    
//    cleanedValue := strings.Trim(val, " ")
//    
//    removable := [4]string{"₦", "N", ",", "\n"}
//    
//    for i := 0; i < len(removable); i++{
//        cleanedValue = strings.Replace(cleanedValue, removable[i], "", -1)
//    }
//    
//    r, _ := regexp.Compile("([0-9]+)")
//    cleanedValue = r.FindString(cleanedValue)
    
//    return cleanedValue
}

func cleanDataPrice(val string) string{
    
    reg, _ := regexp.Compile("[^0-9]")
    
    return reg.ReplaceAllString(val, "")
    
//    cleanedValue := strings.Trim(val, " ")
//    
//    removable := [4]string{"₦", "N", ",", "\n"}
//    
//    for i := 0; i < len(removable); i++{
//        cleanedValue = strings.Replace(cleanedValue, removable[i], "", -1)
//    }
//    
//    r, _ := regexp.Compile("([0-9]+)")
//    cleanedValue = r.FindString(cleanedValue)
    
//    return cleanedValue
}

func getProductFromDivProp(doc *goquery.Document, link string) Product{
    var product Product
    
    doc.Find(".product-name").Each(func(i int, s *goquery.Selection){
        product.Name = cleanTitle(s.Text())
    })
    
    doc.Find(".price").Each(func(i int, s *goquery.Selection){
        
        if i < 2{
            if product.Price == 0.0{
                cleanPrice := cleanDataPrice(s.Text())
                price, _ := strconv.ParseFloat(cleanPrice, 64)
                product.Price = price
            }else{
                newPrice := cleanDataPrice(s.Text())
                priceFloat, _ := strconv.ParseFloat(newPrice, 64)
//                find the lowest price and assign it to product price
                if product.Price > priceFloat{
                    product.Price = priceFloat
                }
            }
        }
        
    })
    
    if product.Price != 0.0 && product.Name != ""{
        product.Link = link
    }
    
    return product
    
}

func getProductFromMicrodata(data *microdata.Microdata, link string) Product{
    var product Product
    
	for i := 0; i < len(data.Items); i++{
		item := data.Items[i]
		offers := item.Properties["offers"]
		
        if len(item.Properties["name"]) > 0{
            product.Name = item.Properties["name"][0].(string);
            product.Name = cleanTitle(product.Name)
        }
        


		for j := 0; j < len(offers); j++{
			offer := offers[j]

			offerItem := offer.(*microdata.Item)
			prop := offerItem.Properties		
            
            var lowestPrice float64
            
            for n := 0; n < len(prop["price"]); n++{
                cleanedPriceString := cleanDataPrice(prop["price"][n].(string))
                cleanedPriceFloat, err := strconv.ParseFloat(cleanedPriceString, 64)
                
                fmt.Println(err)
                if lowestPrice == 0.0{
                    lowestPrice = cleanedPriceFloat
                }else{
                    if cleanedPriceFloat < lowestPrice{
                        lowestPrice = cleanedPriceFloat
                    }
                }
                
            } 
            
            product.Price = lowestPrice
            
		} 
	}
    
    if product.Price != 0.0 && product.Name != ""{
        product.Link = link
    }
    
    return product
}

func GetProductDetail(url string) Product{
    var product Product
    
    data, err := microdata.ParseURL(url)
    
    
    if err == nil{
        product = getProductFromMicrodata(data, url)
        return product
    }
    
    doc, err := goquery.NewDocument(url)
    
    if err == nil{
        product = getProductFromDivProp(doc, url)
        fmt.Println(product)
        return product
    }
    
    
	
    
   

    return product
}

//func main(){
//    
//    fmt.Println(Crawl("http://www.jumia.com.ng/about_us/"))
//    
//}
