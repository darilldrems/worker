package main

import(
    "fmt"
    "worker/links"
    "worker/product"
    "worker/service"
    "strings"
    "net/url"
    "worker/strikeamatch"
    "strconv"
)

type productOffers struct{
    productName string
    productId string
    storeOffers []storeOffer
}

type storeOffer struct{
    store string
    offers []product.Product
}

func getScore()float64{

    return 0.7
}

func searchStoreAndGetRelatedProducts(url string)([]product.Product){
    productLinks := links.GetLinks(url)
    
    products := make([]product.Product, 0)
    
//    c := make(chan product.Product)
    
    for i := 0; i < len(productLinks); i++{
        link := productLinks[i]
        
        rawProduct := product.GetProductDetail(link)
        
        if rawProduct.Price != 0.0 && rawProduct.Name != ""{
            products = append(products, rawProduct)
        }
        
//        go func(link string){
//            c <- product.GetProductDetail(link)
//                
//        }(link)
    
    }
    
//    for j := 0; j < len(productLinks); j++{
//        var rawProduct product.Product
//        
//        rawProduct =<- c
//        
//        if rawProduct.Price != 0.0 && rawProduct.Name != ""{
//            products = append(products, rawProduct)
//        }
//        
//        
//        
//    }
    
    return products
}


func formSearchUrl(searchPath string, productName string) string{
    url := strings.Replace(searchPath, "{{query}}", url.QueryEscape(productName), 1)
    fmt.Println(url)
    return url
}


func filterProducts(productName string, products []product.Product) []product.Product{
    similarityScore := getScore()
    
    similarProducts := make([]product.Product, 0)
    
    for i := 0; i < len(products); i++{
    
        currentProduct := products[i]
        
        similarityRank := strikeamatch.CompareStrings(productName, currentProduct.Name)
       
        fmt.Println("Similarity Score: "+strconv.FormatFloat(similarityRank, 'f', 3, 64))
        if similarityRank >= similarityScore{
        
            similarProducts = append(similarProducts, currentProduct)
            
        }
        
    }
    
    return similarProducts
}


func getProductOffersFromAllStores(product service.Product, stores []service.Store){
    
    
    for i := 0; i < len(stores); i++{
        
        currentStore := stores[i]
        
//        fmt.Println("Current store name is: "+currentStore.SearchPath)
        
        respProducts := searchStoreAndGetRelatedProducts(formSearchUrl(currentStore.SearchPath, product.Name))
            
        
        similarProducts := filterProducts(product.Name, respProducts)

        fmt.Println(similarProducts)

        for j := 0; j < len(similarProducts); j++{

            similarProduct := similarProducts[j]

            //strconv converts float64 to string type
            service.AddOffer(product.Id, currentStore.Id, similarProduct.Link, strconv.FormatFloat(similarProduct.Price, 'f', 0, 64))

        }
        
        
        
        //will try this implementation later
//        go func(store service.Store, prod service.Product){
//            
//            respProducts := searchStoreAndGetRelatedProducts(formSearchUrl(store.SearchPath, prod.Name))
//            
//            similarProducts := filterProducts(prod.Name, respProducts)
//            
//            for j := 0; j < len(similarProducts); j++{
//                
//                product := similarProducts[j]
//                
//                //strconv converts float64 to string type
//                service.AddOffer(prod.Id, store.Id, product.Link, strconv.FormatFloat(product.Price, 'f', 0, 64))
//                
//            }
//            
//        }(currentStore, product)
        
        
    }
    
    
}

func main(){
    
    todo, err := service.GetProductsTodo()
    
    if err != nil{
        fmt.Println("Error found In Get Products")
        return
    }
    
    
    stores, err := service.GetStores()
    
    if err != nil{
        fmt.Println("Error found In Get Stores")
        return
    }
    
//    fmt.Println(stores)
    
    for i := 0; i < len(todo); i++{
    
        product := todo[i]
        
        getProductOffersFromAllStores(product, stores)
    
    }
    
//    result := searchStoreAndGetRelatedProducts("http://www.konga.com/catalogsearch/result/?cat=0&q=iphone+5+16gb")
    fmt.Println("Done------------")
    
//    fmt.Println(strikeamatch.CompareStrings("appleipad2mini64gb", "mac mini i5"))

}
