package service

import (
    "fmt"
    "net/http"
    "errors"
    "encoding/json"
    "io/ioutil"
)

type Store struct{
    Id string `json:"_id"`
    SearchPath string `json:"search_path"`
}

type Product struct{
    Id string `json:"_id"`
    Name string `json:"title"`
}

type ProductsEndpointResponse struct{
    Products []Product `json:"products"`
}

//this response is for stores response endpoint format
type EndpointResponse struct {
    Stores []Store `json:"stores"`
    ItemCount int64 `json:"item_count"`
    PageCount int64 `json:"page_count"`
}

func GetStores() ([]Store, error){
    path := "http://bestprices.ng/backend/backoffice/api/stores?total=50&pass=golang-crawler"
    
    resp, err := http.Get(path)
    
    defer resp.Body.Close()
    
    if err != nil {
        //Todo send error to log on backend backoffice        
        return []Store{}, errors.New("unable to get list of stores from server")
    }
    
    if resp.StatusCode != 200 {
        //Todo send error to log on backend backoffice
        return []Store{}, errors.New("not 200 status code")
    }
    
    //ReadAll returns []byte as response
    response, err := ioutil.ReadAll(resp.Body)

//    println(string(response))
    
    if err != nil {
        return []Store{}, errors.New("content response not readable")
    }
    
    
    var objectResponse EndpointResponse
    
    //Unmarshall takes []byte as content and object reference as struct
    json.Unmarshal(response, &objectResponse)
    
    return objectResponse.Stores, nil
}

func AddOffer(productId string, storeId string, link string, price string){
    
    params := "product="+productId+"&store="+storeId+"&link="+link+"&price="+price+"&pass=golang-crawler"
    
    resp, _ := http.Get("http://bestprices.ng/backend/backoffice/api/offer/new?"+params)
    
    fmt.Println(resp.Body)
    
    defer resp.Body.Close()
        
}

func GetProductsTodo()([]Product, error){
    path := "http://bestprices.ng/backend/backoffice/api/products/todo?pass=golang-crawler"
    
    resp, err := http.Get(path)

    defer resp.Body.Close()
    
    if err != nil{
        return []Product{}, errors.New("unable to fetch products")
    }
    
    var productsResponse ProductsEndpointResponse
    
    if resp.StatusCode != 200{
        return []Product{}, errors.New("response is not 200")
    }
    
    response, err := ioutil.ReadAll(resp.Body)
    
    if err != nil{
        return []Product{}, errors.New("readAll problem is not 200")
    }
    
    
    
    json.Unmarshal(response, &productsResponse)
    
    return productsResponse.Products, nil
}



//func main(){
//    
//    products, _ := GetProductsTodo()
//    fmt.Println(len(products))
//    
//}